<?php
	error_reporting(E_ALL);

    /* DeepBlue Configuration */
    require_once("lib/deepblue.IXR_Library.php");
    require_once("lib/server_settings.php");

    function check_server_status() {
        $client = new IXR_Client(get_server());
	if(!$client->query("echo", '')){

		 /* here we must send an email or log the error */

            // redirect to offline page
            header("Location:  ../offline.php");
        }
    }
?>
